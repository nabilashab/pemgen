# Pemgen

Validates and converts P12 certificate file to PEM file

# Installation

Run `install_pemgen`. If you are unable to run do the following

`cd /location/of/the/script/`

`sudo chmod +x install_pemgen`

# Usage

Open your BASH terminal and enter `pemgen <your_certificate.p12>`.
Validate using the password you chose while exporting the P12 file
PEM certificate is generated in the same directory with the same name (e.g. `your_certificate.pem`)

# License

MIT License. Copyright © 2018 Rayhan Nabi.